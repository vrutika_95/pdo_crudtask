<!DOCTYPE html>
<html>
<head>
	<title>JSON Example</title>
	<meta charset="utf-8">
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		
		<div class="panel">
			<div class="panel-body">
				<p id="demo"></p>
				<p id="demo1"></p>
			</div>
		</div>
	</div>
</body>
</html>
<script type="text/javascript">
	var x="";
	var jobj = {"firstName" : "Vrutika","lastName" : "Rana","age" :  26, "Hobbies" : ["reading","dancing"]};
	for (i in jobj.Hobbies) {
	  x += jobj.Hobbies[i] + ",";
	}
	document.getElementById('demo').innerHTML = "My name is : "+jobj.firstName;
	document.getElementById('demo1').innerHTML = "My Favourtie Hobbies are : "+x;
</script>