<?php
require_once("database.php");

class Methodchaining extends database
{
    private $name;
    private $emil;
    private $age;
    private $test;
    public function __set($setname,$value)
    {
        $this->$setname = $value;
    }

    public function __get($setname){
        return $this->$setname;
    }
    public function setName($name){
        $this->getvar = $name;
    }
    public function getName(){
        return "Welcome" .$this->name.'..!';
    }
    public function get_name($name){
        $this->name = $name;
        return $this;
    }
    public function get_email($email){
        $this->email = $email;
        return $this;
    }

    public function get_age($age){
        $this->age=$age;
        return $this;
    }

    public function getInfo(){
        return 'I am '.$this->name.' and my Email Id is '.$this->email.' and my age is '.$this->age.'.';
    }
}

$dbconn = new Methodchaining();

?>