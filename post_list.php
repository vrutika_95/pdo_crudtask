<?php 

	include_once 'user.class.php';
    
    $result =$dbconn->selectpost();

	if (isset($_GET['delete_id'])) {
		if($dbconn->delete_post($_GET['delete_id']))
		{ 	 
		  	header("Location: post_list.php?deleted");
		}
		else
		{
		  	header("Location: post_list.php?error");
		}
	}
?>
<!DOCTYPE html>
<html>

	<head>
		<title>Post Listing</title>
		<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	</head>

	<body>

		<div class="page-header" align="center">
		  	<h1>Post Listing</h1>
		</div>
		
		<div class="container">
			<div class="panel">
				<a href="list.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon-list"></i> &nbsp; User Listing</a>
				<a href="add_post.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon-plus"></i> &nbsp; Add Post</a>
			</div>
		</div>

		<div class="container">
			<div class="panel panel-default">
				<div class="panel-body">
					<? if(isset($_GET['inserted'])){?>
						<div class="alert alert-success alert-dismissible">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    Record inserted successfully!
						</div>
					<? }else if(isset($_GET['updated'])){?>
						<div class="alert alert-success alert-dismissible">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    Record updated successfully!
						</div>
					<? }else if(isset($_GET['deleted'])){?>
						<div class="alert alert-success alert-dismissible">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    Record Deleted successfully!
						</div>
					<? }else if(isset($_GET['error'])){?>
						<div class="alert alert-warning alert-dismissible">
							<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
						    Something went Wrong!Can not Delete Record!
						</div>
					<? }?>
					<div class="table-responsive">
						<table class="table table-bordered table-striped">
						    <thead>
						      	<tr>
							        <th>User</th>
							        <th>Title</th>
							        <th>Description</th>
						        	<th>Action</th>
						      	</tr>
						    </thead>
						    <tbody>
						    	<?
						    	if(count($result)>0){
						    		foreach($result as $list){
						    			$username = $dbconn->getusername($list->user);
						    		?>
						    	
						      			<tr>
									        <td><?php echo $username['name'];?></td>
									        <td><?php echo $list->title;?></td>
									        <td><?php echo $list->description;?></td>
									        <td>
									        	<a href="update_post.php?edit_id=<?php print($list->id); ?>" title="Edit"><i class="glyphicon glyphicon-edit"></i></a> | 
									        	<a href="detail_post.php?detail_id=<?php print($list->id); ?>" title="Detail"><i class="glyphicon glyphicon-search"></i></a> | 
							                	<a href="post_list.php?delete_id=<?php print($list->id); ?>" title="Delete"><i class="glyphicon glyphicon-trash">
						      				</tr>
						  			<?  } 
						  		}else{?>
							  		<tr>
							  			<td colspan="4">No Data Available..!</td>
							  		</tr>
						  		<? }?>
						    </tbody>
					  	</table>
					</div>
				</div>	
			</div>
		</div>
		
	</body>
</html>