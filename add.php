<?php 
	
	include_once 'user.class.php';
	
	if(isset($_POST['submit'])){
		$name = $_POST['name'];$email = $_POST['email'];$password = $_POST['password'];
		if($dbconn->create($name,$email,$password))
		{ 	 
		  	header("Location: list.php?inserted");
		}
		else
		{
		  	header("Location: add.php?failure");
		}
	}
	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
  		<title>Add User</title>
  		<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
	  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	</head>

	<body>

		<div class="page-header" align="center">
		  	<h1>Add User</h1>
		</div>

		<div class="container">
		 	<? if(isset($_GET['inserted'])){?>
				<div class="alert alert-success alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				    Record inserted successfully!
				 </div>
			<? }else if(isset($_GET['failure'])){?>
				<div class="alert alert-danger alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				    Something went wrong!Try again.!
				 </div>
			<? }?>
		</div>

		<div class="container">
  			<div class="panel panel-default">
  				<div class="panel-body">
  					<form method="post" name="user" action="add.php">
						<div class="form-group">
						    <label for="name">Name</label>
						    <input type="name" class="form-control" name="name" required="required" placeholder="Enter name">
						</div>
					  	<div class="form-group">
						    <label for="email">Email address</label>
						    <input type="email" class="form-control email" name="email" required="required" placeholder="Enter email">
					  	</div>
					  	<div class="form-group">
					    	<label for="password">Password</label>
					    	<input type="password" class="form-control" name="password" required="required" placeholder="Password">
					  	</div>
					  	<div class="form-group">
						  	<button type="submit" class="btn btn-primary" name="submit">Submit</button>
						  	<a href="list.php" class="btn btn-primary" name="submit">Cancel</a>
						</div>
					</form>
  				</div>
			</div>
		</div>

	</body>
</html>