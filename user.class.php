<?php
require_once("database.php");

class User extends database
{
    private $db;
 
 
    public function create($name,$email,$password)
    {
        $addUser = "INSERT INTO tbl_user(name, email, password) values(?,?,?)";
        $this->datamanupulation($addUser, [$name,$email, $password]);
        return true;
  
    }
 
    public function getID($id)
    {
        $stmt = "SELECT * FROM tbl_user WHERE id=:id";
        $data =$this->get($stmt,[':id'=>$id]);
        return $data;
    }
 
    public function update($id,$name,$email,$password)
    {
        $query="UPDATE tbl_user SET name=:name,email=:email,password=:password WHERE id=:id ";
        $this->datamanupulation($query,[':id'=>$id,':name'=>$name,':email'=>$email,':password'=>$password]);
        return true; 
    }
 
    public function delete($id)
    {
        $query="DELETE FROM tbl_user WHERE id=:id";
        $this->datamanupulation($query,[':id'=>$id]);
        return true;
    }
 
 
    public function selectuser()
    {
        $query ='Select * from tbl_user';
        $params = array();
        $result =$this->getall($query,$params);
        return $result;
    }
    public function selectpost()
    {
        $query ='Select * from tbl_post';
        $params = array();
        $result =$this->getall($query,$params);
        return $result;
    }
    public function create_post($user,$title,$description)
    {
        $addUser = "INSERT INTO tbl_post(user, title, description) values(?,?,?)";
        $this->datamanupulation($addUser, [$user,$title, $description]);
        return true;
  
    }
    public function getUsers()
    {
        $query ='Select * from tbl_user';
        $params = array();
        $result =$this->getall($query,$params);
        return $result;
    }
    public function getusername($id)
    {
        $stmt = "SELECT * FROM tbl_user WHERE id=:id";
        $data =$this->get($stmt,[':id'=>$id]);
        return $data;
    }

    public function getPostID($id)
    {
        $stmt = "SELECT * FROM tbl_post WHERE id=:id";
        $data =$this->get($stmt,[':id'=>$id]);
        return $data;
    }
 
    public function update_post($id,$user,$title,$description)
    {
        $query="UPDATE tbl_post SET user=:user,title=:title,description=:description WHERE id=:id ";
        $this->datamanupulation($query,[':id'=>$id,':user'=>$user,':title'=>$title,':description'=>$description]);
        return true; 
    }
 
    public function delete_post($id)
    {
        $query="DELETE FROM tbl_post WHERE id=:id";
        $this->datamanupulation($query,[':id'=>$id]);
        return true;
    }
}

$dbconn = new User();

?>