<?php 
	
	include_once 'user.class.php';
	
	$users = $dbconn->getUsers();
	if(isset($_POST['submit'])){
		$user = $_POST['user'];$title = $_POST['title'];$description = $_POST['description'];
		if($dbconn->create_post($user,$title,$description))
		{ 	 
		  	header("Location: post_list.php?inserted");
		}
		else
		{
		  	header("Location: add_post.php?failure");
		}
	}
	
?>
<!DOCTYPE html>
<html lang="en">
	<head>
  		<title>Add Post</title>
  		<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
	  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
	  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	</head>

	<body>

		<div class="page-header" align="center">
		  	<h1>Add Post</h1>
		</div>

		<div class="container">
		 	<? if(isset($_GET['inserted'])){?>
				<div class="alert alert-success alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				    Record inserted successfully!
				 </div>
			<? }else if(isset($_GET['failure'])){?>
				<div class="alert alert-danger alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				    Something went wrong!Try again.!
				 </div>
			<? }?>
		</div>

		<div class="container">
  			<div class="panel panel-default">
  				<div class="panel-body">
  					<form method="post" name="user" action="add_post.php">
						<div class="form-group">
						    <label for="name">Post</label>
						    <select class="form-control" name="user" required="required">
						    	<option value="">Select User</option>
						    	<? if(count($users)>0){
						    		foreach($users as $user){
						    	?>
						    	<option value="<? echo $user->id;?>"><? echo $user->name;?></option>
						    	<? }}?>
						    </select>
						</div>
					  	<div class="form-group">
						    <label for="title">Title</label>
						    <input type="title" class="form-control" name="title" required="required" placeholder="Enter title">
					  	</div>
					  	<div class="form-group">
						    <label for="description">Description</label>
						    <textarea class="form-control" name="description" required="required" placeholder="Enter description"></textarea>
					  	</div>
					  	<div class="form-group">
						  	<button type="submit" class="btn btn-primary" name="submit">Submit</button>
						  	<a href="post_list.php" class="btn btn-primary" name="submit">Cancel</a>
						</div>
					</form>
  				</div>
			</div>
		</div>

	</body>
</html>