<?php
class database
{
    
    private $host = "localhost:3306";
    private $dbName = "user";
    private $userName = "root";
    private $password = "root";

    private $query;
    function __construct()
    {
        try {
            $this->query = new PDO('mysql:host='.$this->host.';dbname='.$this->dbName, $this->userName, $this->password);
            $this->query->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception) {
            echo $exception->getMessage();
        }
    }

    public function getAll($query, $params = [])
    {
        $statement = $this->query->prepare($query);
        $statement->execute($params);
        $result = $statement->fetchAll(PDO::FETCH_CLASS);
        return $result;
    }

    public function get($query, $params = [])
    {
        $statement = $this->query->prepare($query);
        $statement->execute($params);
        $result = $statement->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function datamanupulation($query, $params)
    {
        $statement = $this->query->prepare($query);
        $statement->execute($params);
    }
}    
?>	