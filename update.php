<?php 
	
	include_once 'user.class.php';

	if(isset($_GET['edit_id'])){ 
		$detail = $dbconn->getID($_GET['edit_id']);
	}
	if(isset($_POST['submit'])){
		$id = $_POST['id'];$name = $_POST['name'];$email = $_POST['email'];$password = $_POST['password'];
		if($dbconn->update($id,$name,$email,$password))
		{
		  	header("Location: list.php?updated");
		}
		else
		{
		  	header("Location: update.php?failure");
		}
	}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
  		<title>Update User</title>
  		<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	</head>

	<body>

		<div class="page-header" align="center">
		  	<h1>Update User</h1>
		</div>

		<div class="container">
		 	<? if(isset($_GET['updated'])){?>
				<div class="alert alert-success alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				    Record updated successfully!
				 </div>
			<? }else if(isset($_GET['failure'])){?>
				<div class="alert alert-danger alert-dismissible">
					<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				    Something went wrong!Try again.!
				 </div>
			<? }?>
		</div>

		<div class="container">
  			<div class="panel panel-default">
  				<div class="panel-body">
  					<form method="post" name="user" action="update.php">
						<div class="form-group">
						    <label for="name">Name</label>
						    <input type="name" class="form-control" name="name" value="<?echo $detail['name'];?>" required="required" placeholder="Enter name">
						</div>
					  	<div class="form-group">
						    <label for="email">Email address</label>
						    <input type="email" class="form-control" name="email" value="<?echo $detail['email'];?>" required="required" placeholder="Enter email">
					  	</div>
					  	<div class="form-group">
					    	<label for="password">Password</label>
					    	<input type="password" class="form-control" name="password" required="required" value="<?echo $detail['password'];?>" placeholder="Password">
					  	</div>
					  	<div class="form-group">
						  	<input type="hidden" name="id" value="<? echo $detail['id'];?>">
						  	<button type="submit" class="btn btn-primary" name="submit">Submit</button>
						  	<a href="list.php" class="btn btn-primary" name="submit">Cancel</a>
						</div>
					</form>
	  			</div>
			</div>
		</div>
		
	</body>
</html>