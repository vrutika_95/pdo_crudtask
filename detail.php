<?php 
	
	include_once 'user.class.php';
	$detail = $dbconn->getID($_GET['detail_id']);
?>
<!DOCTYPE html>
<html lang="en">
	<head>
  		<title>User Details</title>
  		<meta charset="utf-8">
  		<meta name="viewport" content="width=device-width, initial-scale=1">
  		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
	</head>
	<body>

		<div class="page-header" align="center">
	  		<h1> User Details</h1>
		</div>
	
		<div class="container">
  			<div class="panel panel-default">
  				<div class="panel-body">
  					<div class="table-responsive">
						<table class="table table-bordered ">
					    	<tbody>
					    		<tr>
						        	<th>Name</th>
						        	<td><?php echo $detail['name'];?></td>
						      	</tr>
					      		<tr>
							        <th>Email</th>
							        <td><?php echo $detail['email'];?></td>
							    </tr>
					    	</tbody>
				  		</table>
					  	<a href="list.php" class="btn btn-primary" name="submit">Cancel</a>
					</div>
  				</div>
			</div>
		</div>
		
	</body>
</html>